using NUnit.Framework;
// Esta linea nos permite usar codigo del otro proyecto
using FizzBuzz.Logica;

namespace Tests
{
    public class Ejemplo_de_test
    {
        [SetUp]
        public void Setup()
        {
            // Esto es por si quieres configurar algo especial para la prueba
        }

        [Test]
        public void Sumar_numeros_positivos()
        {
            // Arrange
            Ejemplo_logica sujetoPruebas = new Ejemplo_logica();
            int resultado_obtenido = 0;

            // Act
            resultado_obtenido = sujetoPruebas.Suma_basica(1, 1);

            // Assert
            Assert.AreEqual(resultado_obtenido, 1 + 1, "Aviso: resultado inesperado en la prueba de numeros positivos");
        }

        [Test]
        public void Sumar_numeros_negativos()
        {
            // Arrange
            Ejemplo_logica sujetoPruebas = new Ejemplo_logica();
            int resultado_obtenido = 0;

            // Act
            resultado_obtenido = sujetoPruebas.Suma_basica(-1, -1);

            // Assert
            Assert.AreEqual(resultado_obtenido, -1 -1, "Aviso: resultado inesperado en la prueba de numeros negativos");
        }
    }
}